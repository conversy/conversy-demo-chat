from flask import Flask, request, jsonify
from flask_cors import CORS
import socket


app = Flask(__name__)
CORS(app)

CHATBOT_IP = "192.168.0.235"

def chat_once(input):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((CHATBOT_IP, 1024))
    username = b"petlu"
    botname = b"conbot"
    message = input.encode("utf-8")
    payload = username + b"\0" + botname + b"\0" + message + b"\0"
    print("Sending payload...")
    sock.sendall(payload)
    print("Receiving response...")
    response = sock.recv(4096)
    return response


@app.route("/chat", methods=["POST"])
def chat():
	body = request.get_json()
	message = body.get("message")
	print("sending message:", message)
	response = chat_once(message)
	print("response:", response)
	return jsonify({
		"name": response.decode("utf-8"),
		"sender": "bot",
		"id": "X"
	})