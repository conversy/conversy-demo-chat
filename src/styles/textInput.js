import styled from 'styled-components';

import COLORS from './colors';

export const Label = styled.label`
  color: ${COLORS.black};
  font-weight: 500;
  font-size: 13px;
  margin-bottom: 5px;
  display: block;
  cursor: ${({ clickable }) => (clickable ? 'pointer' : 'initial')};
`;

const TextContainer = styled.div`
  margin-bottom: 5px;
  position: relative;

  input {
    background: ${COLORS.white};
    color: ${COLORS.black};
    border: 1px solid ${COLORS.black01};
    border-radius: 6px;
    display: block;
    height: 45px;
    width: 100%;
    outline: none;
    padding: 2px 10px 2px;
    font-size: 17px;
    background-clip: padding-box;
    transition: border 0.2s;

    &:focus {
      border: 1px solid ${COLORS.black02};
    }
  }
`;

const TextInput = ({
  label,
  onChange,
  value,
  id,
  type = 'text',
  placeholder = '',
  focusOnRender,
  className,
  labelFor,
  autoFocus,
  ...props
}) => {
  return (
    <TextContainer className={className} type={type} {...props}>
      {label && (
        <Label htmlFor={labelFor || id} clickable={!!labelFor}>
          {label}
        </Label>
      )}
      <input
        type={type}
        value={value}
        onChange={onChange}
        placeholder={placeholder}
        id={id}
        autoFocus={autoFocus}
      />
    </TextContainer>
  );
};

export default TextInput;
