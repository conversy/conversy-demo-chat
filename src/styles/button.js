import styled from 'styled-components';

import FONTS from './fonts';
import BREAKPOINTS from './breakpoints';
import COLORS from './colors';

export const Rect = styled.button`
  padding: 12px 30px 13px;
  font-size: 14px;
  line-height: 15px;
  border: none;
  color: ${(props) => (props.inverted ? COLORS.black : COLORS.white)};
  background: ${(props) => (props.inverted ? COLORS.white : COLORS.black)};
  border-radius: 5px;
  font-weight: 700;
  font-family: ${FONTS.inter};

  &:hover {
    opacity: 0.9;
    transition: 0.3s;
  }

  &:focus {
    opacity: 0.7;
    transition: 0.3s;
  }

  ${(props) =>
    props.disabled &&
    `
      opacity: 0.5;
      cursor: default;

      &:hover {
        opacity: 0.5;
      }
    `}

  ${BREAKPOINTS.max.small`
    font-size: 15px;
    padding: 8px 10px 9px;
  `}
`;
