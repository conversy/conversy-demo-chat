const colors = {
  white: '#FFFFFF',
  white01: 'rgba(255, 255, 255, 0.1)',
  white015: 'rgba(255, 255, 255, 0.15)',
  white02: 'rgba(255, 255, 255, 0.2)',
  white03: 'rgba(255, 255, 255, 0.3)',
  white035: 'rgba(255, 255, 255, 0.35)',
  white04: 'rgba(255, 255, 255, 0.4)',
  white06: 'rgba(255, 255, 255, 0.6)',
  white09: 'rgba(255, 255, 255, 0.9)',
  black: '#000000',
  black0: 'rgba(0, 0, 0, 0)',
  black01: 'rgba(0, 0, 0, 0.1)',
  black015: 'rgba(0, 0, 0, 0.15)',
  black02: 'rgba(0, 0, 0, 0.2)',
  black04: 'rgba(0, 0, 0, 0.4)',
  black05: 'rgba(0, 0, 0, 0.5)',
  black06: 'rgba(0, 0, 0, 0.6)',
  black09: 'rgba(18, 18, 18, 0.9)',
  lightLightBlue: '#f8f9fd',
  red: '#ed5a70',
};

export default colors;
