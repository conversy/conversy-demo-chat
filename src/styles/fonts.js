import { css } from 'styled-components';

export const loadFonts = css`
  @font-face {
    font-family: 'Inter';
    src: url('/fonts/Inter-Black.ttf');
    font-weight: 900;
  }
  @font-face {
    font-family: 'Inter';
    src: url('/fonts/Inter-ExtraBold.ttf');
    font-weight: 800;
  }
  @font-face {
    font-family: 'Inter';
    src: url('/fonts/Inter-Bold.ttf');
    font-weight: 700;
  }
  @font-face {
    font-family: 'Inter';
    src: url('/fonts/Inter-SemiBold.ttf');
    font-weight: 600;
  }
  @font-face {
    font-family: 'Inter';
    src: url('/fonts/Inter-Medium.ttf');
    font-weight: 500;
  }
  @font-face {
    font-family: 'Inter';
    src: url('/fonts/Inter-Regular.ttf');
    font-weight: 400;
  }
`;

export default {
  inter: 'Inter',
};
