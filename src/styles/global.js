import FONTS, { loadFonts } from './fonts';
import COLORS from './colors';

// reset CSS + injecting fonts
// DO NOT USE THIS FILE TO OVERWRITE STYLING OF CSS CLASSES
const global = `

  ${loadFonts}

  @page {
    margin: 1cm;
  }

  ::selection {
    background: ${COLORS.black};
    color: ${COLORS.white};
  }
  ::-moz-selection {
    background: ${COLORS.black};
    color: ${COLORS.white};
  }

  html, body {
    width: 100%;
    max-width: 100vw;
    overflow-x: hidden;
    margin: 0;
    padding: 0;
    font-family: ${FONTS.inter}, sans-serif;
    font-size: 18px;
    letter-spacing: -0.011em;
    background:  ${COLORS.white};
    transition: background-color 0.2s ease;
    color: ${COLORS.black};
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    
  }

  * {
    box-sizing: border-box;
  }

  a {
    position: relative;
    color: inherit;
    text-decoration: none;
  }

  h1, h2, h3, h4, h5 {
    margin: 0;
    padding: 0;
    font-weight: normal;
  }

  button {
    padding: 0;
    background-color: transparent;
    border: none;
    cursor: pointer;
    outline: none;
    letter-spacing: -0.011em;
  }

  ul, ol {
    list-style: none;
    margin: 0;
    padding: 0;
  }

  li {
    list-style-type: none;
    margin: 0;
    padding: 0;
  }

  input, textarea, button, select {
    font-family: ${FONTS.inter}, sans-serif;
  }
`;

export default global;
