
const SPEECH_PLATFORM_HOST_STT = "192.168.0.126:8080";
const SPEECH_PLATFORM_HOST_VAD = "192.168.0.120:8080";
const VAD_INTERVAL = 1000;
const STT_LANGUAGE = "en";

const VAD_ENDPOINT = `http://${SPEECH_PLATFORM_HOST_VAD}/detect-voice-activity`;
const STT_ENDPOINT = `http://${SPEECH_PLATFORM_HOST_STT}/transcribe/${STT_LANGUAGE}`;

const record = document.querySelector('.record');
//const canvas = document.querySelector('.visualizer');
const mainSection = document.querySelector('.main-controls');

// disable stop button while not recording

// stop.disabled = true;

// visualiser setup - create web audio api context and canvas

let audioCtx;
//const canvasCtx = canvas.getContext("2d");

function transcribe(chunks, callback) {
  console.log("transcribing...");
  const blob = new Blob(chunks, { 'type' : 'audio/ogg; codecs=opus' });
  fetch(STT_ENDPOINT, {
    method: "POST",
    body: blob
  }).then(response => {
    if (response.status === 200) {
      return response.json()
    }
    else {
      return response.text()
    }
  }).then(data => {
    console.log("transcribed!");
    console.log(data[0].confidence,data[0].transcript);
    callback(data[0].transcript, data[0].confidence);
  })
}

function detectVoiceActivity(prevChunks, chunks, activeChunks, onVoiceActivityBegin, onVoiceActivityEnd) {
  const blob = new Blob(chunks, { 'type' : 'audio/ogg; codecs=opus' });
  fetch(VAD_ENDPOINT, {
    method: "POST",
    body: blob
  }).then(response => {
    if (response.status === 200) {
      return response.json()
    }
    else {
      return response.text()
    }
  }).then(data => {
    if (data.channels[0].frames.length > 0) {
      onVoiceActivityBegin();
      //button.style.background = "green";
      if (activeChunks.length === 0) {
        activeChunks.push(prevChunks);
      }
      activeChunks.push(chunks);
    }
    else {
      // record.style.background = "red";
      if (activeChunks.length > 0) {
        activeChunks.push(chunks);
        onVoiceActivityEnd(activeChunks.flat());
      }
      // activeChunks.length = 0; // lol ; https://stackoverflow.com/questions/1232040/how-do-i-empty-an-array-in-javascript
    }
  })
}

export function startSpeechEngine(onVADBegin, onVADEnd, transcriptCallback, setMale,setFemale) {

    if(!audioCtx) {
        audioCtx = new AudioContext();
    }

  if (navigator.mediaDevices.getUserMedia) {
    console.log('getUserMedia supported.');

    const constraints = { audio: true };
    let prevChunks = [];
    let chunks = [];
    let activeChunks = [];

    let onSuccess = function(stream) {

        const mediaRecorder = new MediaRecorder(stream);

        mediaRecorder.ondataavailable = function(e) {
            chunks.push(e.data);
        }

        //visualize(stream, canvasCtx);

        mediaRecorder.onstop = function(e) {
            detectVoiceActivity(prevChunks, chunks, activeChunks, onVADBegin, function(data) {
                onVADEnd();
                transcribe(data, (transcript, confidence) => {
                    if (transcript.length > 0) {
                        transcriptCallback(transcript, confidence)
                    }
                });
                //getGenderEstimation(setMale,setFemale)
                activeChunks = [];
            });
            prevChunks = chunks;
            chunks = [];
        }

        mediaRecorder.start();

        setInterval(() => {
            mediaRecorder.stop();
            mediaRecorder.start();
        }, VAD_INTERVAL)
    }

    let onError = function(err) {
        console.log('The following error occured: ' + err);
    }

    navigator.mediaDevices.getUserMedia(constraints).then(onSuccess, onError);

    } else {
    console.log('getUserMedia not supported on your browser!');
    }

}

