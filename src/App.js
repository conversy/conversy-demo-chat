import voicebio from './assets/VoiceBio-Logo.svg';
import translate from './assets/Translate-Logo.svg';
import transcribe from './assets/Transcription-Logo.svg';
import TextInput from './styles/textInput';
import styled from 'styled-components';
import * as Button from './styles/button';
import './App.css';
import { useEffect, useState } from 'react';
import { startSpeechEngine } from './Speech.js';



const CHATBOT_PROXY_HOST = "192.168.0.120"
const BOT_ENDPOINT = `http://${CHATBOT_PROXY_HOST}:5000/chat`

const EXAMPLE_MESSAGE_LIST = [];
const CONFIDENCE_THRESHOLD = 12


var isSpeechEngineRunning = false;


const EnabledDot = styled.div`
  width: 8px;
  height: 8px;
  margin-right: 5px;
  border-radius: 50%;
  background-color: ${(props) =>
    props.isEnabled ? '#3fd15e' : "#000000"};
`;
const GenderIcon = styled.div`
  width: 30px;
  height: 30px;
  margin-right: 5px;
  background-color: ${(props) =>
    props.gender === 'Male' ? '#000000' : (props.gender === 'Female' ? '#F1F1F1' : '#ed5a70')}
`;



function App() {

  const [new_message, setnewMessage] = useState('');
  const [messageList, setMessageList] = useState(EXAMPLE_MESSAGE_LIST);
  const [isSpeechActive, setisSpeechActive] = useState(false)
  const [gender,setGender] = useState('')
  // const handle = () => console.log('Enter Pressed')

  
  function updateMessageList(message, sender) {
    const arrayvar = messageList.slice();
    arrayvar.push({
      sender: sender,
      name: message,
      id: 'X'
    });
    setMessageList(arrayvar);
    setnewMessage('');
  }

 
  function MessageListUpdater (newelement) {

    const new_object = {name: newelement, id:'X',sender:'Human'}
    const arrayvar = messageList.slice();
    arrayvar.push(new_object);
    setMessageList(arrayvar);
    setnewMessage('');

    fetch(BOT_ENDPOINT, {
      method: "POST",
      body: JSON.stringify({
        message: newelement
      }),
      headers: {
        "Content-Type": "application/json"
      }
    }).then(response => {
      return response.json();
    }).then(data => {
      const arrayvar2 = arrayvar.slice();
      arrayvar2.push(data); 
      setMessageList(arrayvar2);
    }).catch(e => console.error(e))
  }



  function MessageResetter (){
    const arrayvar = messageList.slice(0,0);
    setMessageList(arrayvar);
    setnewMessage('');

    fetch(BOT_ENDPOINT, {
      method: "POST",
      body: JSON.stringify({
        message: ':reset'
      }),
      headers: {
        "Content-Type": "application/json"
      }
    }).then(response => {
      return response.json();
    }).then(data => {
      const arrayvar2 = arrayvar.slice();
      arrayvar2.push(data); 
      setMessageList(arrayvar2);     
    }).catch(e => console.error(e))
  }
/*
  function onKeyPressed(e) {
    console.log(e.key);
  }
  onKeyDown={this.onKeyPressed = (e) => {console.log(e.key)}} tabIndex="-1"
*/

 // .slice(-5, messageList.length)



 useEffect(() => {
  if (!isSpeechEngineRunning){
    isSpeechEngineRunning = true;
    // setisSpeechActive(true);

    function onVADBegin() {
      setisSpeechActive(true)
    }

    function onVADEnd() {
      setisSpeechActive(false)
    }

    function setMale(){
      setGender('Male')
    }
    function setFemale(){
      setGender('Female')
    }
    function delay(time) {
      return new Promise(resolve => setTimeout(resolve, time));
    }

      
    function onTranscript(transcript, confidence) {
      if (confidence < CONFIDENCE_THRESHOLD){
        return
      }
      setMessageList(oldMessageList => {
        const newMessageList = oldMessageList.slice();
        newMessageList.push({
          sender: 'Human',
          name: transcript,
          id: 'X'
        });
        return newMessageList;
      })
      fetch(BOT_ENDPOINT, {
        method: "POST",
        body: JSON.stringify({
          message: transcript
        }),
        headers: {
          "Content-Type": "application/json"
        }
      }).then(response => {
        return response.json();
      }).then(data => {
        setMessageList(oldMessageList => {
          const newMessageList = oldMessageList.slice();
          newMessageList.push(data);
          setisSpeechActive(false);
          return newMessageList;
        })
      }).catch(e => console.error(e))
    }

    startSpeechEngine(onVADBegin, onVADEnd, onTranscript, setMale,setFemale);
    // delay(1000).then(() => console.log('ran after 1 second passed'));
    // setGender(''); //set Neutral Gender
  }
},[isSpeechActive])

function handleKeyPress (ev)  {
  if(ev.key === 'Enter'){
    MessageListUpdater(new_message);
  }
}
  return (
    <div className="App">
      <header className="App-header">
        <h1>Conversy Demo Space</h1>
        <div className="LogoZone">
        <img src={voicebio} className={isSpeechActive ? "App-logo-green" : "App-logo"} alt="logo"/>
        <img src={translate} className="App-logo-rot" alt="logo" />
        <img src={transcribe} className="App-logo-rat" alt="logo" />
        </div>
      </header>
      <content className="App-Content">
        <div className="Chatbox"> 
          {messageList.slice(Math.max(messageList.length-10,0),messageList.length).reverse().map((message) => (
              <div className={`chat-container ${message.sender === "bot" ? "" : "response"}`}>{message.name}</div>
          ))}
        </div>
        <div className="TextContainerWithButton"> 
          <TextInput
            value={new_message}
            onChange={(e) => setnewMessage(e.target.value)}
            type="message"
            placeholder='Type or Talk to bot....'
            onKeyPress={(e)=> handleKeyPress(e)}
            />
            
          <Button.Rect onClick={() => {
                    MessageListUpdater(new_message);
                  }}>Send</Button.Rect>
          <Button.Rect onClick={()=> {
            MessageResetter();
          }}
          >Restart</Button.Rect>
        </div>
      </content>
    </div>
  );
}

export default App;
